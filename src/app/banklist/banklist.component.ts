import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-banklist',
  templateUrl: './banklist.component.html',
  styleUrls: ['./banklist.component.css']
})
export class BanklistComponent implements OnInit {

  selected = ''
  searchText;
  banks:any = [];
  loading = true;
  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.httpClient.get('https://vast-shore-74260.herokuapp.com/banks?city=MUMBAI').subscribe((res) => {
      console.log('url data',res)
      this.banks = res
      this.loading = false
      console.log('banks data', this.banks);
    })
  }

  onChangeCity(){
    this.loading = true;
    this.httpClient.get('https://vast-shore-74260.herokuapp.com/banks?city='+this.selected).subscribe((res)=>{
      this.banks = res
      this.loading = false
      console.log('bank data',this.banks)
    })
  }

}
